package com.manipulations.controller;

import static org.junit.Assert.assertEquals;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.manipulations.ArithmeticManipulationsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ArithmeticManipulationsApplication.class)
@WebAppConfiguration
public class ArithmeticManipulationControllerTest {

	protected MockMvc mockMvc;

	@Autowired
	WebApplicationContext context;

	@Mock
	HttpServletRequest request;

	@InjectMocks
	ArithmeticManipulationController controller = new ArithmeticManipulationController();

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testHappyPathSumOfGivenNumbers() throws Exception {

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.get("/addnumbers?numbers=" + "1,2,3,4").contentType("text/plain"))
				.andReturn();

		String sumOfNumbers = result.getResponse().getContentAsString();
		assertEquals(sumOfNumbers, "HTTP 200 OK \nThe sum of the numbers = " + 10);
	}

	@Test
	public void testNegativePathSumOfGivenNumbers() throws Exception {

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.get("/addnumbers?numbers=" + "1").contentType("text/plain"))
				.andReturn();

		String sumOfNumbers = result.getResponse().getContentAsString();
		assertEquals(sumOfNumbers, "HTTP 400 Bad Request");
	}

}