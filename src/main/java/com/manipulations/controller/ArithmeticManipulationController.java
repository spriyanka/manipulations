package com.manipulations.controller;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArithmeticManipulationController {

	@GetMapping("/addnumbers")
	public ResponseEntity<String> sumOfGivenNumbers(@RequestParam String[] numbers) {
		
		if(numbers.length== 1) {
			return new ResponseEntity<>("HTTP 400 Bad Request", HttpStatus.BAD_REQUEST);
		}

		if (numbers.length== 0) {
			return new ResponseEntity<>("HTTP 400 Bad Request", HttpStatus.BAD_REQUEST);
		}
		
		int sumOfNumbers = Arrays.stream(numbers).mapToInt(temp -> Integer.parseInt(temp)).sum();

		return new ResponseEntity<>(" HTTP 200 OK " +" \n The sum of the numbers = "+sumOfNumbers  , HttpStatus.OK);
	}

}
