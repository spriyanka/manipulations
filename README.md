# manipulations

The default port for the service is 8080 

 We can run JAR file as shown below:

java -jar target/manipulations 0.0.1-SNAPSHOT.jar

**Postman URL to test this Microservice : **

Happy Path:  

http://localhost:8080/addnumbers?numbers=1,133,1

Response: 
 HTTP 200 OK  
 The sum of the numbers = 135

Negative Scenarios (missing one number or no numbers at all ):

http://localhost:8080/addnumbers?numbers=1

Response:
HTTP 400 Bad Request
